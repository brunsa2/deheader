/*
 * Items: closedir(
 * Standardized-By: SuS
 * Detected-by: gcc-4.4.3 + Linux
 */

#include <sys/types.h>
#include <dirent.h>

main(int arg, char **argv)
{
    (void)closedir(0);
}
